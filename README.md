## Synopsis

A javascript framework for creating google-streetview based point and click adventure games.

## Screenshot
[![Screenshot from 2017-06-19 12-38-47.png](https://s8.postimg.org/iwy43uh39/Screenshot_from_2017-06-19_12-38-47.png)](https://postimg.org/image/d8rtcycqp/)
[![Screenshot from 2017-06-28 11-36-47.png](https://s1.postimg.org/i4mx782tr/Screenshot_from_2017-06-28_11-36-47.png)](https://postimg.org/image/4ayki6a8b/)

## Documentation
Go to your project directory and open it in your terminal and type:
```bash
npm install
```
Which installs jsdoc in your local project.

To generate the documentation files run:
```
npm run doc
```
After that browse to the $PROJECTFOLDER/doc/jsdoc.

see 'How to use JSDOC.md' for more information.

## Contributors
* Jan Julius de Lang
*  Arthur van den Broek
*  Peter van der Velde
*  Thomas Tijsma
*  Leon Schubert

## License
See 'Licence.md'.
