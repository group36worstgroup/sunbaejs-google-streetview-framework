/**
 * Creates a minimap.
 * @name Minimap
 * @constructor
 * @author Thomas Tijsma
 * @author Peter vd Velde
 * @author Arthur van den Broek
 */
function MiniMap(markerImgPath, startLocation) {
  
  // Coordinates to center the map
  var myLatlng = startLocation;
  var panorama = map.getStreetView();
  // street view control
  var controlDiv = document.getElementById("miniMap");
  var streetViewLayer = new google.maps.StreetViewCoverageLayer();

  mapControl = new google.maps.Map(controlDiv, {
    zoom: 16,
    center: myLatlng,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    draggable: false,
    mapTypeControl: false,
    zoomControl: false,
    scrollwheel: false,
    rotateControl: false,
    scaleControl: false,
    panControl: false,
    streetViewControl: false
  });

  // 
  var pegLord = {
    url: 'image/playerMarkerPurple.png',
    scaledSize: new google.maps.Size (22, 22)
  }

  // Place a draggable marker on the map
  var myPegman = new google.maps.Marker({
    map: mapControl,
    draggable: false,
    visible: true,
    icon: pegLord,
    animation: google.maps.Animation.DROP,
    title: "Player",
  });



  /*
   * events over my pegman 
   * need to check streetview availability before changing the
   * panorama
   */
  google.maps.event.addListener(myPegman, "dragend", function () {
    panorama.setPosition(myPegman.getPosition());
    streetViewLayer.setMap(null);
  });

  google.maps.event.addListener(myPegman, "dragstart", function () {
    streetViewLayer.setMap(mapControl);
  });

  google.maps.event.addListener(panorama, "visible_changed", function () {
    google.maps.event.trigger(mapControl, "resize");
    myPegman.setPosition(panorama.getPosition());
  });

  google.maps.event.addListener(panorama, "position_changed", function () {
    mapControl.setCenter(panorama.getPosition());
    myPegman.setPosition(panorama.getPosition());
  });

  panorama.controls[google.maps.ControlPosition.LEFT_BOTTOM].push(controlDiv);
  map.setStreetView(panorama);
}