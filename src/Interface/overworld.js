var player = new Player("PlaceholderName");
var map;
var startLoc = { lat: 53.21169, lng: 5.79687 };
var quests = new Array(1);

function initMap() {
    // Set up the map
    map = new google.maps.Map(document.getElementById('map'), {
        center: startLoc,
        zoom: 18,
        mapTypeControl: false,
        streetViewControl: false
    });

    quests[0] = new WorldQuest(0, { lat: 53.21169, lng: 5.79687 }, map, "image/QuestSymbol.png", new google.maps.Size(64, 64), "Quest: demo", "demo.html");
    quests[0].setName("demo quest");
    quests[0].setDiscription("Dit is een demo quest om te laten zien wat het sunbaeJS framework allemaal kan doen.");
    quests[0].setDoing("Voor deze quest moet je de onderdelen van de power ranger pak voor leon vinden.");


    if (sessionStorage.getItem("questId") != -1 && sessionStorage.getItem("questStatus") == "done") {
        new Dialog(new QuestCompleted(quests[sessionStorage.getItem("questId")].getName())).run();
        quests[sessionStorage.getItem("questId")].setStatus(true);
        sessionStorage.setItem("questId", -1);
        sessionStorage.setItem("questStatus", "");
    }


}