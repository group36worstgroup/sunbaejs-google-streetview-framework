var div = document.createElement('div');
div.innerHTML = "my <b>new</b> skill - <large>DOM maniuplation!</large>";
// set style
div.style.width = '100px';
div.style.height = '100px';
div.style.background = 'blue';
div.style.color = 'blue';
div.innerHTML = 'Hello';

// better to use CSS though - just set class
div.setAttribute('class', 'myclass'); // and make sure myclass has some styles in css
document.body.appendChild(div);
