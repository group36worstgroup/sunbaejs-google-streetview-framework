function swordUse(slotid) {
    let test = "oops";

    function swordDialogue() {

        this.dial = function (stage, npc) {
            if (stage == -1) {
                console.log(slotid);
                inventory.removeAt(slotid);
                inventory.add(bloodySword, 1);
                return { "name": "", "dialogue": "je hebt jezelf gesneden! " + test, "type": "single", "chathead": ironSword.getImgPath() }
            }
            else {
                return null;
            }
        }

    }
    let dialogue = new Dialog(new swordDialogue());

    dialogue.run();
}

function bananaPeel(slotid) {
    inventory.add(peeledBanana, 1);
    inventory.mySlots[slotid].decr(1);
}

function Cleancloth(slotid) {

    function ClothDialogue() {

            this.dial = function (stage, npc) {
                if (stage == -1) {
                    inventory.removeItem(10, 1);
                    inventory.add(ironSword, 1);
                    return { "name": "", "dialogue": "Je gebruikt het doekje om je zwaard schoon te maken, nu kan je het zwaard weer gebruiken!", "type": "single", "chathead": cleaningCloth.getImgPath() }
                } else {
                    return null;
                }
            }
        }

    if (inventory.contains(10)) {
        let dialogue = new Dialog(new ClothDialogue());

        dialogue.run();
    }
}