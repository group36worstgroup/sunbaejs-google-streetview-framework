var locations = [{ lat: 53.2119248, lng: 5.8014746 }, {lat: 53.215153, lng: 5.796624}, {lat: 53.206897, lng: 5.794367}]

/**
 * @desc calculates distance between the player and the given marker
 * @param {google.maps.marker} marker - calculate between this point
 * @returns {int} returns the distance
 */
function calcDistance(marker) {

    let slatlongMap = map.getStreetView().getPosition().toString();
    let slatlongMarker = marker.getPosition().toString();

    let locArray = slatlongMarker.replace(/[\])}[{(]/g, '').split(',');
    let guessArray = slatlongMap.replace(/[\])}[{(]/g, '').split(',');

    return Math.ceil(google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(locArray[0], locArray[1]), new google.maps.LatLng(guessArray[0], guessArray[1])));
};

/**
 * @desc returns the shortened term of a large value
 * @param {int} amount - amount to convert
 * @returns html code for shortened term
 */
function displayAmountText(amount) {
    if (amount > 1000000000) {
        return "<font color=\"lime\">" + Math.floor(amount / 1000000000) + "B" + "</font>";
    } else if (amount > 1000000) {
        return "<font color=\"cyan\">" + Math.floor(amount / 1000000) + "M" + "</font>";
    } else if (amount > 1000) {
        return "<font color=\"white\">" + Math.floor(amount / 1000) + "K" + "</font>";
    } else {
        return "<font color=\"white\">" + amount + "</font>";
    }
}

/**
 * @desc Checks if you clicked on an element
 * @param {event} e - the event
 * @param {string} className - element classname
 * @returns element that is clicked if nothing is clicked returns false
 */
function clickInsideElement(e, className) {
    var el = e.srcElement || e.target;

    if (el.classList.contains(className)) {
        return el;
    } else {
        while (el = el.parentNode) {
            if (el.classList && el.classList.contains(className)) {
                return el;
            }
        }
    }

    return false;
}

/**
 * @desc returns x position of the cursor during the event
 * @param {event} evt - occuring event
 * @returns x position, if nothing is pressed null 
 */
function mouseX(evt) {
    if (evt.pageX) {
        return evt.pageX;
    } else if (evt.clientX) {
        return evt.clientX + (document.documentElement.scrollLeft ?
            document.documentElement.scrollLeft :
            document.body.scrollLeft);
    } else {
        return null;
    }
}

/**
 * @desc returns y position of the cursor during the event
 * @param {event} evt - occuring event
 * @returns y position, if nothing is pressed null 
 */
function mouseY(evt) {
    if (evt.pageY) {
        return evt.pageY;
    } else if (evt.clientY) {
        return evt.clientY + (document.documentElement.scrollTop ?
            document.documentElement.scrollTop :
            document.body.scrollTop);
    } else {
        return null;
    }
}