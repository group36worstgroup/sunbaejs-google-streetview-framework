
class Button{
 /**
 * Creates a button
 * @constructor
 * @author Leon Schubert
 * @author Jan Julius de Lang
 * @deprecated
 * @param {string} canvas the html class where the button is
 * @param {number} posx change position x of the button
 * @param {number} posy change position y of the button
 * @param {string} content The name of the button
 * @param {string} id the html id of the button
 * @param {string} styleclass the html id of the button
 */   
    constructor(canvas, posx, posy, content, action, id, styleclass){
        var style = styleclass;
        var clickevent = action;
        canv = document.getElementById(canvas);
        canv.innerHTML = canv.innerHTML + "<button class=\"" + style + "\"" + 
        "type=\"button\"" + "id=\"" + id + "\""  + "></button>";
        canv.onclick = action;
        var thisObj = document.getElementById(id);
        thisObj.innerHTML = content;
        this.move(thisObj, posx, posy);
    }

    /**
     * Moves the button
     * @param {string} obj the html id of the button
     * @param {number} posx change position x of the button
     * @param {number} posy change position y of the button
     * @author Leon Schubert
     */
    move(obj, posx, posy){
        obj.style.position = "absolute";
        obj.style.left = posx.toString() + "px";
        obj.style.top = posy.toString() + "px";
    }
     /**
     * Removes the button -- it does not remove the button, never would, never will, this not how JavaScript works or for that matter anything.
     * @author Leon Schubert
     */
    removebutton() {
        canv = null;
        thisObj = null;
    }
};

