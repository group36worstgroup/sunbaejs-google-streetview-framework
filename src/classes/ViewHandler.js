    /**
      * Creates a new viewhandler
      * @constructor
      */
function ViewHandler(){
    /**
     * Returns the result of the parameters
     * @param {geoLocation} loc the geolocation. 
     * @param {string} s  size of the view window
     * @param {int} h the x axis of the view
     * @param {int} p the y axis of the view
     * @param {int} sca scaling
     * @param {int} f field of view of the view
     */
    this.createviewresult = function(loc, s = '600x300', h, p, sca = 2, f = 90){
        let imagePath = 'https://maps.googleapis.com/maps/api/streetview?size=' 
        + s 
        +'&location=' + loc 
        + '&heading=' + h.toString() 
        + '&pitch=' + p.toString() 
        + '&scale=' + sca.toString() 
        + '&fov=' + f.toString();
        return imagePath;
    }
    
}
