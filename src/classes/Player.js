/**
 * @desc Creates a new player
 * @constructor
 * @author Jan Julius de Lang
 * @param {string} parName Name of the player
 */
function Player(parName) {
    this.name = parName;

    this.handedOverHelm = false;
    this.handedOverBody = false;
    this.handedOverLegs = false;
    this.handedOverGloves = false;
    this.handedOverBoots = false;
    this.bredeWillem = false;

    /**
     * @returns name of this character
     */
    this.getName = function () {
        return this.name;
    }

    /**
     * @returns if the player has given the helmet to Leon
     */
    this.getHandedOverHelm = function () {
        return this.handedOverHelm;
    }
    /**
     * @returns if the player has given the body to Leon
     */
    this.getHandedOverBody = function () {
        return this.handedOverBody;
    }
    /**
     * @returns if the player has given the legs to Leon
     */
    this.getHandedOverLegs = function () {
        return this.handedOverLegs;
    }
    /**
     * @returns if the player has given the gloves to Leon
     */
    this.getHandedOverGloves = function () {
        return this.handedOverGloves;
    }
    /**
     * @returns if the player has given the boots to Leon
     */
    this.getHandedOverBoots = function () {
        return this.handedOverBoots;
    }

    /**
     * @returns if WIllem or Brede Willem
     */
    this.getbredeWillem = function () {
        return this.bredeWillem;
    }

    /**
     * @desc set if the player has handed over the helm to leon
     * @param {boolean} b - boolean to set to
     */
    this.setHandedOverHelm = function (b) {
        this.handedOverHelm = b;
    }


    /**
 * @desc set if the player has handed over the body to leon
 * @param {boolean} b - boolean to set to
 */
    this.setHandedOverBody = function (b) {
        this.handedOverBody = b;
    }
    /**
 * @desc set if the player has handed over the legs to leon
 * @param {boolean} b - boolean to set to
 */
    this.setHandedOverLegs = function (b) {
        this.handedOverLegs = b;
    }
    /**
 * @desc set if the player has handed over the gloves to leon
 * @param {boolean} b - boolean to set to
 */
    this.setHandedoverGloves = function (b) {
        this.handedOverGloves = b;
    }
    /**
 * @desc set if the player has handed over the boots to leon
 * @param {boolean} b - boolean to set to
 */
    this.setHandedOverBoots = function (b) {
        this.handedOverBoots = b;
    }

    /**
     * @desc Sets Willem to Brede Willem
     */
    this.setbredeWillem = function (b) {
        this.bredeWillem = b;
    }

    /**
     * @desc checks if the set was completed by the player
     * @returns true if the set is complete else false
     */
    this.completedPowerRangerSet = function () {
        return this.handedOverHelm &&
            this.handedOverBody &&
            this.handedOverLegs &&
            this.handedOverGloves &&
            this.handedOverBoots;
    }

    /**
     * @desc sets the ranger piece to boolean value
     * @param {int} i - id of the item
     * @param {boolean} b - to set to
     */
    this.gotRangerPiece = function (i, b) {
        switch (i) {
            case 2:
                this.handedOverHelm = b;
                break;
            case 3:
                this.handedOverBody = b;
                break;
            case 4:
                this.handedOverLegs = b;
                break;
            case 5:
                this.handedOverGloves = b;
                break;
            case 6:
                this.handedOverBoots = b;
                break;

        }
    }

    //admin commands
    this.warp = function (number) {
        map.getStreetView().setPosition(locations[number]);
    }


    this.completeSet = function () {
        this.handedOverHelm = true;
        this.handedOverBody = true;
        this.handedOverLegs = true;
        this.handedOverGloves = true;
        this.handedOverBoots = true;
    }
}
