/**
 * Contains the account information
 * @constructor
 * @param {string} parName acount name 
 * @param {string} parPassword  account password
 * @param {Player} parPlayer the player object
 * @param {string} parEmail emailaddress
 * @param {string} parNickname accounts nickname
 */
function Account(parName, parPassword, parPlayer, parEmail, parNickName) {
    let loginname = parName;
    let password = parPassword;
    let player = parPlayer;
    let email = parEmail;

    /**
     * returns player
     * @return player
     */
    function getPlayer() {
        return player;
    }
}
