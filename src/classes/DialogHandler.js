var activeDialog = null;

/**
 * @desc goes to the next dialogue
 */
function nextDialog() {
    activeDialog.run();
}

/**
 * @desc sets the dialogue stage to given parameter
 * @param {int} s - sets the stage to this parameter
 */
function setStage(s) {
    activeDialog.stage = s;
    activeDialog.run();
}

let dialogueInterface = "<div id=\"" + "dialogue-floating-panel" + "\">" + "</div>";
document.body.innerHTML = document.body.innerHTML + dialogueInterface;
let thisDialogueElement = document.getElementById("dialogue-floating-panel");
thisDialogueElement.style.visibility = "hidden";

/**
 * @desc Runs the dialogue that is given through the parameter
 * @author Jan Julius de Lang
 * @author Arthur van den Broek
 * @param {dialogue} parDialogue - dialogue to go through 
 */
this.Dialog = function (parDialogue) {
    this.stage = -1;
    this.curDialogue = parDialogue;
    this.npc;

    /**
     * @desc Runs the given dialogue
     */
    this.run = function () {
        activeDialog = this;

        document.getElementById("dialogue-floating-panel").style.visibility = "visible";
        let myDialogue = this.curDialogue.dial(this.stage, this.npc);
        if (myDialogue !== null) {
            if (myDialogue.type == "single") {
                let di = new DialogueInterface(myDialogue.name, myDialogue.dialogue, myDialogue.chathead);
                di.draw();
                if (typeof myDialogue.newstage != 'undefined') {
                    this.stage = myDialogue.newstage;
                } else {
                    this.stage++;
                }
            } else if (myDialogue.type == "multi") {
                let doi = new DialogueOptionInterface(myDialogue.question, myDialogue.options, myDialogue.stages, myDialogue.chathead);
                doi.draw();
            } else if (myDialogue.type == "setter") {
                this.stage = myDialogue.newstage;
                activeDialog.run();
            }
        } else {
            document.getElementById("dialogue-floating-panel").style.visibility = "hidden";
            activeDialog = null;
            this.stage = -1;
        }
    }

    /**
     * @desc sets the dialoguehandler's npc
     * @param {WorldNPC} parNpc - npc to set to
     */
    this.setNpc = function(parNpc){
        this.npc = parNpc
    }
}

/**
 * @desc Creates a regular dialogue interface for a message and a name
 * @param {string} name - name of the npc you're talking to
 * @param {string} content - message that is being sent
 */
this.DialogueInterface = function (name, content, chathead) {

    /**
     * @desc draws the interface
     */
    this.draw = function () {
        let thisDialogueElement = document.getElementById("dialogue-floating-panel");
        thisDialogueElement.style.display = "block";
        thisDialogueElement.innerHTML = "<div id=\"" + "dialogueInterface" + "\">" +
            "<p id=\"" + "dialogueInterfaceName" + "\">" +
            "</p>" +
            "<p id=\"" + "dialogueInterfaceContent" + "\">" +
            "</p>" +
            "<img id=\"" + "dialogueInterfaceChathead" + "\"" + "src=\"" + chathead + "\"" + "\">" +
            "</img>" +
            "<button id=\"dialogButton\" class=\"interfaceDialogButton\" onclick=\"nextDialog()\">Go on</button>" +
            "</div>";

        document.getElementById("dialogueInterfaceName").innerHTML = name;
        document.getElementById("dialogueInterfaceContent").innerHTML = content;
        return 0;
    }
}

/**
 * @desc draws a multiple choice options interface
 * @param {string} question - question that appears at the top of the interface
 * @param {string[]} options - array of options as string ex ["yes", "no"]
 * @param {int[]} stages - stages that the answers lead you to
 */
this.DialogueOptionInterface = function (question, options, stages, chathead) {

    /**
     * @desc draws the interface
     */
    this.draw = function () {
        let thisDialogueElement = document.getElementById("dialogue-floating-panel");
        thisDialogueElement.innerHTML = "<div id =\"" + "dialogueOptionInterface" + "\">" + "<p id=\"" + "dialogueOptionInterfaceQuestion" + "\"" + "</p>" + "<p id=\"" + "dialogueOptionInterfaceOptions" + "\"" + "</p>" + "<img id=\"" + "dialogueInterfaceChathead" + "\"" + "src=\"" + chathead + "\"" + "\">" +
            "</img>" + "</div>";
        document.getElementById("dialogueOptionInterfaceQuestion").innerHTML = question;
        let optionsArea = document.getElementById("dialogueOptionInterfaceOptions");
        for (var i = 0; i < options.length; i++) {
            optionsArea.innerHTML = optionsArea.innerHTML + "<button class=\"interfaceDialogButton\" id=\"" + "dialogueOtionsInterfaceOption" + i + "\"" + "onclick=\"setStage(" + stages[i] + ")\">" + options[i] + "</button> <br>";
        }
    }
}