/**
 * Creates a warp point
 * @author Jan Julius de Lang
 * @constructor
 */
function Warp() {

    /**
     * Draws the marker
     * @param {google.maps.position} position -  represents the geolocation of the marker (lat: x, lng: x; as object)
     * @param {google.maps.map} map - represents the map to draw the marker in
     * @param {string} imgPath - link to the image of the marker
     * @param {google.maps.size} size - size of the marker (new google.maps.Size(x, y))
     * @param {string} title - title of the warp
     * @param {google.maps.position} goto - represents the new geolocation, after clicking on the marker
     * @param {boolean} condition - the condition needs to be true to let the warp happen.
     */
    this.draw = function (postion, map, imgPath, size, title, goto, condition) {
        this.map = map;
        let img = {
            url: imgPath,
            scaledSize: size
        };

        marker = new google.maps.Marker({
            position: postion,
            map: map,
            icon: img,
            title: title
        });

        marker.addListener('click', function () {
            if (condition) {
                map.setCenter(goto);
            }
        });
    }

}