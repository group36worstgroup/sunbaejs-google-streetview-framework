/**
 * Create a world npc object
 * @constructor
 * @author Arthur van den Broek
 * @author Peter van der Velde
 * @param {Dialog} parDialog The dialog of the npc
 * @param {string} parName The name of the npc
 * @param {number} parSizex The size of the x axis
 * @param {number} parSizey The size of the y axis
 */
function WorldNPC(dialog, name, sizex, sizey, position, map, imgPath) {
    let marker;
    let minimapMarker;

    dialog.setNpc(this);

    /**
     * @desc removes npc
     * @param {object} args - type of removing
     */
    this.remove = function (args) {
        switch (args.type) {
            case "death":
                if (args.dropping) {
                    args.dropTable.forEach(function (element) {
                        var dodekees = new WorldItem(element.item, element.amount).draw(position.lat, position.lng, map);
                        minimapMarker.setMap(null);
                        marker.setMap(null);
                    }, this);
                }

                break;

            default:
                console.log("ik ben weg");
                break;

        }

    }

    let img = {
        url: imgPath,
        scaledSize: new google.maps.Size(sizex, sizey)
    };

    let imgMarker = {
        url: 'image/npcMarco.png',
        scaledSize: new google.maps.Size(20, 20)
    };
    // Set up the markers on the map
    marker = new google.maps.Marker({
        position: position,
        map: map,
        icon: img,
        title: name
    });
    minimapMarker = new google.maps.Marker({
        position: position,
        map: mapControl,
        icon: imgMarker,
        title: name
    });
    marker.addListener('click', function () {
        let distance = calcDistance(marker);
        if (distance < 15) {
            if (activeDialog == null) {
                dialog.run();
            }
        }
    });


    google.maps.event.addListener(map.getStreetView(), "position_changed", function () {

        if (activeDialog == dialog) {
            activeDialog.stage = -2;
            activeDialog.run();
        }


        let distance = calcDistance(marker);

        if (distance > 50) {
            marker.setVisible(false);
        } else {
            if (!marker.getVisible())
                marker.setVisible(true);

            if (distance < 15) {
                let img = {
                    url: marker.getIcon().url,
                    scaledSize: new google.maps.Size(sizex * 2, sizey * 2)
                };
                marker.setIcon(img);
            } else if (marker.getIcon().scaledSize.width > sizex) {
                let img = {
                    url: marker.getIcon().url,
                    scaledSize: new google.maps.Size(sizex, sizey)
                };
                marker.setIcon(img);
            }
        }
    });
};