/**
 * @desc creates a worldobject
 * @constructor
 * @author Jan Julius de Lang
 * @param {object} func - function of the object (clickevent)
 * @param {object} objType - type of object and information
 * @param {int} positionx - position on x axis
 * @param {int} positiony -position on y axis
 * @param {int} sizex - size on x axis
 * @param {int} sizey - size on y axis
 * @param {map} map - map
 * @param {string} pic  - link of img
 * @param {string} name - name of object
 */
function WorldObject(func, objType, positionx, positiony, sizex, sizey, map, pic, name) {

    let img = {
        url: pic,
        scaledSize: new google.maps.Size(sizex, sizey)
    };
    // Set up the markers on the map
    marker = new google.maps.Marker({
        position: { lat: positionx, lng: positiony },
        map: map,
        icon: img,
        title: name
    });
    marker.addListener('click', function () {
        if (func == undefined) {
            this.func = null;
        } else {
            if (objType.objectMode == "charge") {
                if (objType.charges > 0) {
                    func.function();
                    objType.charges--;
                    var a = (objType.charges - 1);
                    if (a == 0) {

                        let img = {
                            url: objType.depletedImg, scaledSize: new google.maps.Size(sizex, sizey)
                        }
                        marker.setIcon(img);
                    }
                }
            }
            else {
                this.func = func;
            } //else if(objType.objectMode == "new mode"){}
        }
    });

    google.maps.event.addListener(map.getStreetView(), "position_changed", function () {

        let distance = calcDistance(marker);

        if (distance > 50) {
            marker.setVisible(false);
        } else {
            if (!marker.getVisible())
                marker.setVisible(true);
        }
    });


};

