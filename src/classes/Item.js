/**
 * Represents an item, collectable in the inventory and can be a worlditem
 * @constructor
 * @author Jan Julius de Lang
 * @author Arthur van den Broek
 * @param {string} parName - Name of the item
 * @param {boolean} parStackable - Wether the item is stackable or not
 * @param {string} parImgpath - Path to the item image
 * @param {int} parId - id of the item
 * @param {int} sizeX - width of the item
 * @param {int} sizeY - Height of the item
 */
function Item(parName, parStackable, parImgpath, parId, sizex, sizey, desc, useFunction) {
    this.name = parName;
    this.stackable = parStackable;
    this.imagepath = parImgpath;
    this.id = parId;
    this.size = { x: sizex, y: sizey };
    this.description = desc;
    this.useOpt = "gebruik";

    if (useFunction == undefined) {
        this.useFunction = null;
    } else {
        this.useFunction = useFunction;
    }

    /**
     * @returns Name of the item
     */
    this.getName = function () {
        return this.name;
    }

    /**
     * @desc Sets the name of the item to param n
     * @param {string} n - Name to set the item's name to
     */
    this.setName = function (n) {
        this.name = n;
    }

    /**
     * @returns true if the item is stackable else false
     */
    this.getStackable = function () {
        return this.stackable;
    }

    /**
     * @desc Sets the stackable value of the item to param s
     * @param {bool} s - Wether the item is stackable or not
     */
    this.setStackable = function (s) {
        this.stackable = s;
    }

    /**
     * @returns Image path of the item
     */
    this.getImgPath = function () {
        return this.imagepath;
    }

    /**
     * @returns id of the item
     */
    this.getId = function () {
        return this.id;
    }

    /**
     * @returns id of the item
     */
    this.getSize = function () {
        return this.size;
    }

    /**
     * @returns description of this item
     */
    this.getDescription = function () {
        return this.description;
    }

    /**
     * @desc uses this items function
     * @param {int} slotId - slot that the item is in
     */
    this.use = function (slotid) {
        console.log("used " + this.name);
        if (this.useFunction != null) {
            useFunction(slotid);
        }
    }

    /**
     * @desc sets the use option to param
     * @param {string} opt - what the new use option is
     */
    this.setUseOption = function(opt){
        if (typeof opt != 'undefined')
            this.useOpt = opt;
    }

    /**
     * @returns use option of the item
     */
    this.getUseOption = function(){
        return this.useOpt;
    }

}
