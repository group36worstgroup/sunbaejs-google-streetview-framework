/**
 * @description A worldquest, creates a marker that will create a clickable link to your given url, must call draw function to render on the google maps
 * @author Jan Julius de Lang
 * @constructor
* @param {google.maps.position} position -  represents the geolocation of the marker (lat: x, lng: x; as object)
* @param {google.maps.map} map - represents the map to draw the marker in
* @param {string} imgPath - link to the image of the marker
* @param {google.maps.size} size - size of the marker (new google.maps.Size(x, y))
* @param {string} title - hover title of the marker
* @param {string} gameUrl - url to go to when the marker is clicked
 */
function WorldQuest(questId, postion, map, imgPath, size, title, gameUrl) {
    let discription = "";
    let questDoing = "";
    let name = "";
    let completed = false;
    let dialogue = new Dialog(new questDialogue());
    let img = {
        url: imgPath,
        scaledSize: size
    };

    marker = new google.maps.Marker({
        position: postion,
        map: map,
        icon: img,
        title: title
    });

    marker.addListener('click', function () {
        dialogue.run();
        //window.location.href = gameUrl;
    });

    function questDialogue() {

        this.dial = function (stage, npc) {

            if (stage == -1) {
                return { "name": name, "dialogue": discription, "type": "single", "chathead": imgPath }
            }
            else if (stage == 0) {
                return { "name": name, "dialogue": questDoing, "type": "single", "chathead": imgPath }
            }
            else if (stage == 1 && !completed) {
                return { "question": "wilt u nu de quest beginnen?", "options": ["ja", "nee"], "stages": [100, -2], "type": "multi", "chathead": imgPath }
            }else if(stage == 1 && completed){
                return { "name": name, "dialogue": "Je hebt deze quest al gedaan!", "type": "single", "chathead": imgPath }
            }
            else if (stage == 100) {
                sessionStorage.setItem("questId",questId);
                window.location.href = gameUrl;
            } else {
                return null;
            }
        }
    }

    this.setName = function (n) {
        name = n;
    }

    this.setDiscription = function (d) {
        discription = d;
    }
    this.setDoing = function (d) {
        questDoing = d;
    }

    this.getName = function(){
        return name;
    }

    this.setStatus = function(s){
        completed = s;
    }
}