/**
 * @description Creates an inventory
 * @author Jan Julius de Lang
 * @author Arthur van den Broek
 * @constructor
 * @param {int} size - size of the inventory, amount of slots that are created stackable items will only take too slots and have no limit
 */
function Inventory(size) {

    this.size = size;
    this.mySlots = new Array(size);
    const standardSlotSize = 36;

    //fills inventory with empty slots
    for (var i = 0; i < this.size; i++) {
        this.mySlots[i] = new itemSlot(new Item(null, true, "", 0, 0, 0), 0, i);
    }

    /**
     * @description adds an item to the inventory
     * @param {Item} parItem - item to add to the inventory
     * @param {int} quant - quantity of the item to add to the inventory 
     * if the quantity is more than 1 and the item is not stackable this will automatically be corrected
     */
    this.add = function (parItem, quant) {
        for (var i = 0; i < this.size; i++) {
            if (this.mySlots[i].getItem().getId() == parItem.getId()) {
                if (this.mySlots[i].getItem().getStackable()) {
                    this.editSlot(i, this.mySlots[i].getItem(), this.mySlots[i].getAmount() + quant);
                    return 0;
                } else {
                    continue;
                }
            }
            else if (this.mySlots[i].getItem().getName() == null) {
                this.editSlot(i, parItem, quant);
                if (!parItem.getStackable() && quant > 1) {
                    this.add(parItem, quant - 1);
                }

                return 0;
            }
        }
        var leftOver = new WorldItem(parItem, quant).draw(map.getStreetView().getPosition().lat(), map.getStreetView().getPosition().lng(), map);
    }

    /**
     * @description checks if an item of the given id exists in the inventory
     * @param {int} id - id to check for
     * @returns true if exists else false
     */
    this.contains = function (id) {
        for (var i = 0; i < this.size; i++) {
            if (id == this.mySlots[i].getItem().getId()) {
                return true;
            }
        }
        return false;
    }

    /**
     * @desc gives the slot of the item that is asked
     * @param {int} id - id to search for
     * @returns slot of item
     */
    this.getSlotOfItem = function (id) {
        for (var i = 0; i < this.size; i++) {
            if (id == this.mySlots[i].getItem().getId()) {
                return this.mySlots[i];
            }
        }
    }

    /**
     * @description Checks the amount of the given id in the inventory
     * @param {int} id= id of the item you are looking for
     * @returns amount of given item id
     */
    this.containsAmountOf = function (id) {
        for (var i = 0; i < this.size; i++) {
            if (id == this.mySlots[i].getItem().getId()) {
                return this.mySlots[i].getAmount();
            }
        }
    }

    /**
     * @description Removes item from the inventory that has the given id
     * @param {int} id - id of the item that you want to remove from the inventory
     * @param {int} quant - amount of item sto remove
     */
    this.removeItem = function (id, quant) {
        for (var i = 0; i < this.size; i++) {
            if (id == this.mySlots[i].getItem().getId()) {
                this.mySlots[i].decr(quant);
                this.updateAmount(i);
                return 0;
            }
        }
    }

    /**
     * @description Removes the item from the inventory and then drops it on the ground at the current location
     * @param {int} id - id of the item to drop
     * @param {int} quant - quantity of the item to drop
     */
    this.drop = function (id, quant) {
        for (var i = 0; i < this.size; i++) {
            if (id == this.mySlots[i].getItem().getId()) {
                new WorldItem(this.mySlots[i].getItem(), quant).draw(map.getStreetView().getPosition().lat(), map.getStreetView().getPosition().lng(), map);
                this.mySlots[i].decr(quant);
                this.updateAmount(i);
                return 0;
            }
        }
    }

    /**
     * @desc drops all current items in the inventory
     */
    this.dropAll = function () {
        for (var i = 0; i < this.size; i++) {
            if (this.mySlots[i].getItem().getName() !== null) {
                this.mySlots[i].drop();
            }
        }
    }

    /**
     * @description Removes item at the given index and clears the slot entirely
     * @param {int} index - index of the inventory to remove the item from
     */
    this.removeAt = function (index) {
        this.mySlots[index].clearSlot();
        this.updateAmount(index);
    }

    /**
     * @description Clears the entire inventory
     */
    this.clearAll = function () {
        for (var i = 0; i < this.size; i++) {
            this.mySlots[i].clearSlot();
        }
    }

    /**
     * @desc checks if the inventory has space
     * @returns true if there is space else false
     */
    this.hasInventorySpace = function () {
        for (var i = this.size; i >= 0; i--) {
            if (mySlots[i].getName() == null) {
                return true;
            }
        }
        return false;
    }

    /**
     * @description Edit the slot at iven slot index
     * @param {int} slotIndex - index to edit at
     * @param {Item} parItem - Item to change the slot into
     * @param {int} amount - amount of the item that you are adding
     */
    this.editSlot = function (slotIndex, parItem, amount) {
        this.mySlots[slotIndex] = new itemSlot(parItem, amount, slotIndex);
        this.updateAmount(slotIndex);

    }

    /**
     * @description Updates all items in the inventory with the given id
     * @param {int} id - id of items to update
     */
    this.updateAmount = function (parId) {
        var amount = this.mySlots[parId].getAmount();
        var slot = document.getElementById("inventSlot" + parId);

        if (amount == 0) {
            slot.innerHTML = "<img src=\"" + this.mySlots[parId].getItem().getImgPath() + "\">";
        } else
            slot.innerHTML = "<img src=\"" + this.mySlots[parId].getItem().getImgPath() + "\">" + "<div id=\"" + "amountText" + parId + "\"" + "class=\"" + "amountText" + "\"" + ">" + displayAmountText(amount) + "</img>";
    }

    /**
     * @desc Draws the inventory
     */
    this.draw = function () {
        document.body.innerHTML = document.body.innerHTML + "<div id=\"invent-floating-panel\"></div>";
        var floatPanel = document.getElementById("invent-floating-panel");
        //floatPanel.style.top = "10px";
        //floatPanel.style.width = standardSlotSize * this.size + "px";
        floatPanel.innerHTML = floatPanel.innerHTML + "<div id=\"sunbaeInventory\"></div>";
        var invent = document.getElementById("sunbaeInventory");
        invent.style.top = "70%";
        invent.style.left = "10%";
        let current = 0;
        for (var i = 0; i < this.size; i++) {
            invent.innerHTML = invent.innerHTML + "<div id=\"inventSlot" + i + "\" class=\"inventSlot\"></div>";
            var thisSlot = document.getElementById("inventSlot" + i);

            if (this.mySlots[i].getItem().getId() == 0) {
                thisSlot.innerHTML = thisSlot.innerHTML + "<img src=\"" + this.mySlots[i].getItem().getImgPath() + "\"></img>";
            } else {
                thisSlot.innerHTML = thisSlot.innerHTML + "<img src=\"" + this.mySlots[i].getItem().getImgPath() + "\">" + "<div id=\"" + "amountText" + i + "\"" + "class=\"" + "amountText" + "\"" + ">" + displayAmountText(this.mySlots[i].getAmount()) + "</img>";
                var amountSlot = document.getElementById("amountText" + i);
                amountSlot.innerHTML = this.mySlots[i].getAmount();

            }
            thisSlot.style.float = "left";
        }

        document.body.innerHTML = document.body.innerHTML + "  <div class=\"hide\" id=\"rmenu\"><ul><div id=\"rmenu-items\"><li><a id=\"rmenu-use\">gebruik</a></li><li><a id=\"rmenu-examine\">bekijk</a></li><li><a id=\"rmenu-drop\">gooi weg</a></li></div></ul></div>";
    }
}

/**
 * @desc Creates an itemslot object
 * @author Jan Julius de Lang
 * @author Arthur van den Broek
 * @param {Item} parItem - Item to put in the slot
 * @param {int} a - Amount of the item to put in the slot
 *  @constructor
 */
function itemSlot(parItem, a, parId) {
    this.myItem = parItem;
    this.id = parId;

    this.amount = this.myItem.getStackable() ? a : 1;

    /**
     * @desc Drops the item in the slot at the location of the player
     */
    this.drop = function () {
        if (this.myItem.getName() != null) {
            console.log("dropped " + this.myItem.getName());
            var droppedItem = new WorldItem(this.myItem, this.amount).draw(map.getStreetView().getPosition().lat(), map.getStreetView().getPosition().lng(), map);
            this.clearSlot();
            inventory.updateAmount(this.id);
        }
    }

    /**
     * @desc examines the item in the slot
     */
    this.examine = function () {
        if (this.myItem.getName() != null) {

            let dialogue = new Dialog(new ExamineItem(this.myItem, this.amount, this.myItem.getImgPath()));

            dialogue.run();
        }
    }

    /**
     * @desc clears the current slot
     */
    this.clearSlot = function () {
        this.amount = 0;
        this.myItem = new Item(null, 0, "", 0, 0, 0);
    }

    /**
     * @desc increases the amount of this item by 1
     * @param {int} by - how many to increase by
     */
    this.incr = function (by) {
        if (this.myItem.getStackable()) {
            this.amount = this.amount + by;
        }
    }

    /**
     * @desc decreases the amount of this item by 1 or clears the slot when the item has a quantity of 1
     * @param {int} by - how many to decrease by
     */
    this.decr = function (by) {
        if ((this.amount - by) <= 0) {
            this.amount = 0;
            this.clearSlot();
            inventory.updateAmount(this.id);
        } else {
            this.amount = this.amount - by;
            inventory.updateAmount(this.id);
        }
    }

    /**
     * @desc returns this slots item
     * @returns this slot's item
     */
    this.getItem = function () {
        return this.myItem;
    }

    /**
     * @desc returns the amount of this slot
     * @returns amount of this slot
     */
    this.getAmount = function () {
        return this.amount;
    }

}

document.addEventListener("contextmenu", function (e) {
    var taskItemInContext = clickInsideElement(e, "inventSlot");

    if (taskItemInContext) {
        let idd = parseInt(taskItemInContext.id.toString().replace("inventSlot", ''));
        e.preventDefault();
        if (inventory.mySlots[idd].myItem.getName() != null) {

            document.getElementById("rmenu").className = "show";
            document.getElementById("rmenu").style.top = mouseY(event) + 'px';
            document.getElementById("rmenu").style.left = mouseX(event) + 'px';

            document.getElementById("rmenu-drop").onclick = function () { inventory.mySlots[idd].drop() };
            document.getElementById("rmenu-examine").onclick = function () { inventory.mySlots[idd].examine() };
            let rmenuse = document.getElementById("rmenu-use");
            rmenuse.onclick = function () { inventory.mySlots[idd].getItem().use(idd) };
            rmenuse.innerHTML = inventory.mySlots[idd].myItem.getUseOption();
        }
    }
});

document.addEventListener("click", function (e) {
    document.getElementById("rmenu").className = "hide";
});
