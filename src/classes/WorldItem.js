/**
 * @desc Creates a world item
 * @constructor
 * @author Arthur van den Broek
 * @param {Item} parItem - item
 * @param {int} parAmount - hoeveelheid items
 */
function WorldItem(parItem, parAmount) {
    let item = parItem;
    let marker;
    let amount = parAmount;
    /**
     * @desc Draws the item on the world map
     * @param {double} positionx the x coordinate on the world map
     * @param {double} positiony the y coordinate on the world map
     * @param {Map} map the map the item is drawn on
     */
    this.draw = function (positionx, positiony, map) {
        let img = {
            url: item.getImgPath(),
            scaledSize: new google.maps.Size(item.getSize().x, item.getSize().y)
        };
        // Set up the markers on the map
        marker = new google.maps.Marker({
            position: { lat: positionx, lng: positiony },
            map: map,
            icon: img,
            title: item.getName()
        });
        marker.addListener('click', function () {
            inventory.add(item, amount);
            marker.setMap(null);
        });

        google.maps.event.addListener(map.getStreetView(), "position_changed", function () {

            let distance = calcDistance(marker);

            if (distance > 50) {
                marker.setVisible(false);
            } else {
                if (!marker.getVisible())
                    marker.setVisible(true);
            }
        });

    };
};

