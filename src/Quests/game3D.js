var inventory = new Inventory(36);
var map;
var startLoc = { lat: 53.21169, lng: 5.79687 };

var banana = new Item("Banaan", true, "image/Items/Banana.gif", 1, 64, 64, banaanDesc);
var ironSword = new Item("zwaard", false, "image/Items/IronSword.png", 2, 64, 64, swordDesc, { "function": swordUse, "args": { "test": "oops" } });
var bloodySword = new Item("zwaard met bloed", false, "image/Items/BloodySword.png", 2, 64, 64, bloodySwordDesc);
var runeHelm = new Item("Helm", false, "image/Items/RuneHelm.png", 3, 41, 64), runeHelmDesc;
var coin = new Item("coin", true, "image/Items/Coin.png", 4, 40, 40, coinDesc);
var dodeKees = new Item("dode kees", false, "image/Items/DodeKees.png", 3434, 228, 228, dodeKeesDesc);
var gangstaBanana = new Item("Ganster Banaan", false, "image/Items/GangstaBanana.png", 43594, 32, 32, gangstaDesc);

/**
 * Set up the map
 */
function initMap() {
  inventory.draw();

  // Set up the map
  map = new google.maps.Map(document.getElementById('map'), {
    center: startLoc,
    zoom: 18,
  });

  MiniMap();

  var panoramaOptions = {
    disableDefaultUI: true
  };

  map.getStreetView().setOptions(panoramaOptions);
  map.getStreetView().setPosition(startLoc);
  map.getStreetView().setVisible(true);

  var testItem1 = new WorldItem(banana, 1).draw(53.21195, 5.79818, map);
  var testItem2 = new WorldItem(ironSword, 1).draw(53.21240, 5.79764, map);
  var testItem3 = new WorldItem(runeHelm, 1).draw(53.21260, 5.79704, map);
  var testItem4 = new WorldItem(coin, 2000000).draw(53.21193, 5.79685, map);

  var questTest = new WorldQuest().draw({ lat: 53.2008317, lng: 5.7977071 }, map, "https://www.sythe.org/data/avatars/l/516/516927.jpg?1466179354", new google.maps.Size(64, 64), "Quest: Spellekein", "Spellekein.html");

  var sjaak = new WorldNPC(new Dialog(new Sjaak()), "Sjaak", 120, 209, { lat: 53.211150, lng: 5.796572 }, map, "image/Arthoer.png");
  var fred = new WorldNPC(new Dialog(new Fred()), "Fred", 70, 100, { lat: 53.211465, lng: 5.796652 }, map, "image/budget goblin.png");
  var willem = new WorldNPC(new Dialog(new Willem()), "willem", 64, 100, { lat: 53.21166, lng: 5.79714 }, map, "image/Willem.png");
  var kees = new WorldNPC(new Dialog(new Kees()), "kees", 135, 150, { lat: 53.21233, lng: 5.79775 }, map, "image/Kees.png");
  var paardman = new WorldNPC(new Dialog(new Paardman()), "paardman", 175, 250, { lat: 53.21206, lng: 5.79703 }, map, "image/Paardman.png");
  //needs testing
  var newWarp = new Warp().draw({ lat: 54.21210, lng: 5.79818 }, map, "https://png.icons8.com/material/1600/00897B/exit", new google.maps.Size(64, 64), "Warp to: Paris", ({ lat: 40.7143528, lng: -74.0059731 }, true));

}