/**
 * Quest: game
 * Style: 2D 
 */


//styling
let sunbody = document.getElementById('SunBody');
let buttonLeft = document.getElementById('buttonLeft');
let buttonRight = document.getElementById('buttonRight');
let sb = sunbody.style;

sb.backgroundImage = "url('https://maps.googleapis.com/maps/api/streetview?size=600x300&location=53.1304288,6.0556961&heading=0&pitch=10&scale=2&fov=90')";
buttonLeft.innerHTML = "⇦";
buttonRight.innerHTML = "⇨";
setElementStyles();

let aButton = new Button().draw("SunBody", 100, 100, 100, 100, "test123", "move(true)", "newButton", "btn waves-effect waves-light");


function move(left){
    //create a new viewhandler
    var vh = new ViewHandler();
    if(left){
    var b = vh.createviewresult("58.2614261,7.9634635", "600x300", 0, 10, 2, 90);
    sb.background = "url('" + b + "')";
    setElementStyles();
}
else {
    var b = vh.createviewresult("53.2120106,5.7988923", "600x300", 0, 10, 2, 90);
    sb.background = "url('" + b + "')";
    setElementStyles();
    }
}

function setElementStyles(){
    sb.width = "1200px";
    sb.height = "600px";
    sb.backgroundRepeat = "no-repeat";
    sb.margin = "100px 100px 100px 100px";
    sb.backgroundSize = "100%";
}