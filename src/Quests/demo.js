var player = new Player("PlaceholderName");
var inventory = new Inventory(6);
var map;
var startLoc = { lat: 53.21169, lng: 5.79687 };

//items
var banana = new Item("Banaan", true, "image/Items/Banana.gif", 1, 64, 64, banaanDesc, bananaPeel);
banana.setUseOption("peel");
var powerRangerHelm = new Item("Power Ranger helm", false, "image/Items/Powerranger/helm.png", 2, 64, 64, powerRangerBodyDesc);
var powerRangerBody = new Item("Power Ranger shirt", false, "image/Items/Powerranger/body.png", 3, 64, 64, powerRangerBodyDesc);
var powerRangerLegs = new Item("Power Ranger broek", false, "image/Items/Powerranger/legs.png", 4, 64, 64, powerRangerLegsDesc);
var powerRangerGloves = new Item("Power Ranger handschoenen", false, "image/Items/Powerranger/gloves.png", 5, 64, 64, powerRangerGlovesDesc);
var powerRangerBoots = new Item("Power Ranger boots", false, "image/Items/Powerranger/boots.png", 6, 64, 64, powerRangerBootsDesc);
var ironSword = new Item("Zwaard", false, "image/Items/IronSword.png", 7, 64, 64, swordDesc, swordUse);
ironSword.setUseOption("doe stoer");
var runeHelm = new Item("Rune helm", false, "image/Items/RuneHelm.png", 8, 41, 64, runeHelmDesc);
var coin = new Item("Munt", true, "image/Items/Coin.png", 9, 40, 40, coinDesc);
var bloodySword = new Item("Zwaard met bloed", false, "image/Items/BloodySword.png", 10, 64, 64, bloodySwordDesc);
var peeledBanana = new Item("Geschilde Banaan", true, "image/Items/Peeledbanana.png", 11, 40, 40, peeledBananaDesc);
peeledBanana.setUseOption("eat");
var cleaningCloth = new Item("Doekje", false, "image/Items/CleaningCloth.png", 12, 40, 40, cleaningClothDesc, Cleancloth);
var peanut = new Item("Pinda", true, "image/Items/Pinda.png", 13, 40, 40, peanutDesc);
var dodeKees = new Item("Dode Kees", false, "image/Items/DodeKees.png", 3434, 228, 228, dodeKeesDesc);
var gangstaBanana = new Item("Ganster Banaan", false, "image/Items/GangstaBanana.png", 43594, 32, 32, gangstaDesc);

console.log(banana);

function initMap() {
    inventory.draw();
    // Set up the map
    map = new google.maps.Map(document.getElementById('map'), {
        center: startLoc,
        zoom: 18,
    });


    MiniMap('image/playerMarkerPurple.png', startLoc);

    var panoramaOptions = {
        disableDefaultUI: true
    };

    var bozeArthurDialog = new Dialog(new Wortel());
    bozeArthurDialog.run();

    map.getStreetView().setOptions(panoramaOptions);
    map.getStreetView().setPosition(startLoc);
    map.getStreetView().setVisible(true);

    var Sword = new WorldItem(ironSword, 1).draw(53.21240, 5.79764, map);
    var Helm = new WorldItem(runeHelm, 1).draw(53.211477, 5.796961, map);

    var sjaak = new WorldNPC(new Dialog(new Sjaak()), "Sjaak", 80, 80, { lat: 53.211111, lng: 5.796634 }, map, "image/Arthoer.png");
    var fred = new WorldNPC(new Dialog(new Fred()), "Fred", 70, 100, { lat: 53.211465, lng: 5.796652 }, map, "image/budget goblin.png");
    var willem = new WorldNPC(new Dialog(new Willem()), "Willem", 64, 100, { lat: 53.21166, lng: 5.79714 }, map, "image/Willem.png");
    var kees = new WorldNPC(new Dialog(new Kees()), "Kees", 135, 150, { lat: 53.21233, lng: 5.79775 }, map, "image/Kees.png");
    var paardman = new WorldNPC(new Dialog(new Paardman()), "Paardman", 175, 250, { lat: 53.21206, lng: 5.79703 }, map, "image/Paardman.png");
    var leon = new WorldNPC(new Dialog(new Leon()), "Leon", 100, 100, {lat: 53.2118722, lng: 5.8013370}, map, "image/leon.png");
    var jos = new WorldNPC(new Dialog(new Jos()),"Jos",150,50,{lat: 53.211857,lng: 5.796725 }, map, "image/Deadpool.png");
    var yuki = new WorldNPC(new Dialog(new Yuki()), "Yuki", 50, 150, {lat: 53.206897, lng: 5.794367}, map, "image/bodypillow.png");
    var bananaTree = new WorldObject({"function": pickBananaTree}, {"objectMode":"charge", "charges": 5, "depletedImg": "image/Objects/tropicalbananatreedepleted.png"}, 53.21228, 5.79752, 661, 1071, map, "image/Objects/tropicalbananatree.png", "Bananen boom"); 
    
}