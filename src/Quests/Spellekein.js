var panorama;

      function initMap() {
        var astorPlace = {lat: 53.21194, lng: 5.79745};

        // Set up the map
        var map = new google.maps.Map(document.getElementById('map'), {
          center: astorPlace,
          zoom: 18,
        });
        var img = { 
                    url: "http://i.imgur.com/JeOkVcW.png",
                    scaledSize: new google.maps.Size(25, 25)
                  };
        // Set up the markers on the map
        var cafeMarker = new google.maps.Marker({
            position: {lat: 53.21195, lng: 5.79818},
            map: map,
            icon: img,
            title: 'Cafe'
        });

        cafeMarker.addListener('click', function(){

            alert('TRIHARD');
        })

        // We get the map's default panorama and set up some defaults.
        // Note that we don't yet set it visible.
       /// panorama = map.getStreetView();
       // panorama.setPosition(astorPlace);
        panorama.setPov(/** @type {google.maps.StreetViewPov} */({
          heading: 265,
          pitch: 0
        }));
      }

      function toggleStreetView() {
        var toggle = panorama.getVisible();
        if (toggle == false) {
          panorama.setVisible(true);
        } else {
          panorama.setVisible(false);
        }
      }