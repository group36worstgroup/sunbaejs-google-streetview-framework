/**
 * dis zeit meine gutte friend Fred der Goblin
 * @author God
 * @author Petrus
 */
function Fred() {
    this.n = "Fred";
    this.l = "image/goblinchathead.png";

    this.dial = function (stage) {
        if (stage == -1) {
            return { "name": this.n, "dialogue": "Ich bin Fred der goblin! Haha. Heil der goblinführer", "type": "single", "chathead": this.l }
        }
        else if (stage == 0) {
            return { "name": this.n, "dialogue": "Alle Menschen müssen sterben, ich verhöre Kees! Er hat Bananen van den Nationen gestohlen. Heil der goblinführer", "type": "single", "chathead": this.l }
        }
        else if (stage == 1) {
            return { "question": "Bringt mir Kees seinen Körper und ihn bananen Oder ich werde deine Dörfer verbrennen und deine Frauen vergewaltigen. Heil der goblinführer", "options": ["Jawohl", "Nein"], "stages": [100, 200], "type":"multi", "chathead": this.l  }
        } else if (stage == 100) {
            return { "name": this.n, "dialogue": "Gut, der goblinführer wird das nie vergessen.", "type": "single", "chathead": this.l }
        } else if(stage == 101 && inventory.contains(3434) && inventory.contains(1, 3000)){
            let bananaName = inventory.getSlotOfItem(1).getItem().getName();
            let bananaChat = inventory.getSlotOfItem(1).getItem().getImgPath();
            let gbName = inventory.getSlotOfItem(3434).getItem().getName();
            let gbChat = inventory.getSlotOfItem(3434).getItem().getImgPath();
            return { "name": "", "dialogue": "Du übergibst " + gbName + " und " + bananaName + "  zum goblin. Heil der goblinführer", "type": "single", "chathead": gbChat }
        }else if (stage == 101 && !inventory.contains()){
            return {"name": this.n, "dialogue": "Du entartende schweinhund! Wo ist ihn körper und ihn bananen den? Heil der goblinführer", "type": "single", "chathead": this.l } 
        } else if (stage == 102 && inventory.contains(3434)) {
            let amountOfBananas = inventory.containsAmountOf(banana.getId());
            inventory.removeItem(banana.getId(), amountOfBananas);
            inventory.removeItem(dodeKees.getId(), 1);
            inventory.add(powerRangerBody);
            inventory.add(coin, 56 * amountOfBananas);
            return { "name": this.n, "dialogue": "Ah! Danke, dass du mir die Leiche des Entarteten und den bananen gebracht hast, Heil der goblinführer", "type": "single", "chathead": this.l }
        }else if (stage == 200) {
            return { "name": this.n, "dialogue": "Du dumme Hure ich fütterst Sie zu den Hunden, wo Sie den verdammten Nerv erhalten Sie, nein zu sagen? Heil der goblinführer", "type": "single", "chathead": this.l }
        }
        else {
            return null;
        }
    }
}