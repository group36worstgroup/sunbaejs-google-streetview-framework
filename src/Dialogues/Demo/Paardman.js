function Paardman() {
    this.l = "http://i.imgur.com/caaTsSa.png";

    let horseChoice; //1 = stone, 2 = paper, 3 = scissors
    let horseChoiceString;

    this.dial = function (stage, npc) {
        if (stage == -1) {
            if (inventory.contains(43594)) {
                return { "name": "Paardman", "dialogue": "Je hebt al een gangster banaan.. je hoeft mij niet nog een keer te vernederen", "type": "single", "chathead": this.l, "newstage": -2 }
            }
            return { "name": "Paardman", "dialogue": "Als jij van mij wint in steen papier schaar krijg jij van mij een gangster banaan!", "type": "single", "chathead": this.l }
        } else if (stage == 0) {
            return { "question": "Wil je steen papier schaar spelen met Paardman?", "options": ["Ja", "Nee"], "stages": [100, 200], "type": "multi", "chathead": this.l }
        } else if (stage == 100) { //picked yes
            return { "name": "Paardman", "dialogue": "Laten we spelen dan! kies steen, papier of schaar!", "type": "single", "chathead": this.l }
        } else if (stage == 101) {
            horseChoice = Math.floor((Math.random() * 3) + 1);
            switch (horseChoice) {
                case 1:
                    horseChoiceString = "Steen";
                    break;
                case 2:
                    horseChoiceString = "Papier";
                    break;
                case 3:
                    horseChoiceString = "Schaar";
                    break;
            }
            return {
                "question": "Kies 1 van de opties", "options": ["<img src=\"http://www.ahristov.com/taller/procgraph/rocks/result0.gif\"/>", "<img src=\"http://i.imgur.com/dzJTE80.png\"/>", "<img src=\"http://i.imgur.com/9LtGzen.png\"/>"],
                "stages": [300, 400, 500], "type": "multi", "chathead": this.l
            }
        } else if (stage == 300) { //picked stone
            switch (horseChoice) {
                case 1:
                    return { "type": "setter", "newstage": 600 }
                case 2:
                    return { "type": "setter", "newstage": 800 }
                case 3:
                    return { "type": "setter", "newstage": 700 }
            }
        } else if (stage == 400) { //picked paper
            switch (horseChoice) {
                case 1:
                    return { "type": "setter", "newstage": 700 }
                case 2:
                    return { "type": "setter", "newstage": 600 }
                case 3:
                    return { "type": "setter", "newstage": 800 }
            }
        } else if (stage == 500) { //picked scissors
            switch (horseChoice) {
                case 1:
                    return { "type": "setter", "newstage": 800 }
                case 2:
                    return { "type": "setter", "newstage": 700 }
                case 3:
                    return { "type": "setter", "newstage": 600 }
            }
        }
        else if (stage == 600) { //tie
            return { "name": "Paardman", "dialogue": "IK HAD OOK " + horseChoiceString.toUpperCase() + "! IK HAD MOETEN WINNEN", "type": "single", "chathead": this.l }
        } else if (stage == 700) { // win
            return { "name": "Paardman", "dialogue": "NEE IK HAD " + horseChoiceString.toUpperCase() + ", IK MOEST WINNEN NEEEEEEEEEEEEEE", "type": "single", "chathead": this.l }
        } else if (stage == 701) {
            inventory.add(gangstaBanana, 1);
            return { "name": "Paardman", "dialogue": "Hier heb je je gangster banaan..", "type": "single", "chathead": this.l }
        }
        else if (stage == 800) { //lose
            return { "name": "Paardman", "dialogue": "PAK AAN SMIECHT IK HAD " + horseChoiceString.toUpperCase() + ",HAHAHA JE ZAL NOOIT DE GANGSTER BANAAN KRIJGEN HAHAHAHHA!!!", "type": "single", "chathead": this.l }
        }
        else if (stage == 601) {
            return { "question": "Wil je nog een keer spelen?", "options": ["Ja!", "Nee kutpaard!"], "stages": [100, -2], "type": "multi", "chathead": this.l }
        }
        else if (stage == 200) { //picked no
            return { "name": "Paardman", "dialogue": "Je durft niet he! watje!", "type": "single", "chathead": this.l }
        }
        else {
            return null;
        }
    }
}