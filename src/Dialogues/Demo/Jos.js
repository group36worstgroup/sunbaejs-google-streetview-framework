function Jos() {
    this.n = "Jos";
    this.l = "image/deadpoolchathead.png";

    this.dial = function (stage, npc) {
        if (stage == -1) {
            return { "name": this.n, "dialogue": "Hoi ik ben " + this.n + ", welkom bij de Schoenenwinkel", "type": "single", "chathead": this.l }
        } else if (stage == 0 && !inventory.contains(9)) {
            return { "name": this.n, "dialogue": "Ik zie dat je geen geld hebt, opdonderen pauper.", "type": "single", "chathead": this.l }
        } else if (stage == 0 && inventory.contains(9)) {
            return { "name": this.n, "question": "Ah ik zie dat je geld hebt! wil je handeldrijven?", "type": "multi", "options": ["ja", "ja", "graag"], "stages": [1, 1, 1], "chathead": this.l }
        }
        else if (stage == 1 && inventory.contains(9)) {
            return {
                "name": this.n, "question": "Waar kan ik je blij mee maken?", "options": ["<img src=\"http://i.imgur.com/ItSc3rq.png\"/>",
                    "<img src=\"http://i.imgur.com/H8RIPpZ.png\"/>", "EC's", "<img src=\"http://i.imgur.com/4dlcRXM.png\"/>"], "stages": [100, 200, 300, 400], "type": "multi", "chathead": this.l
            }
        }
        else if (stage == 100 && inventory.containsAmountOf(coin.getId()) >= 279998) {
            inventory.removeItem(coin.getId(), 279998)
            inventory.add(powerRangerBoots, 1)
            return { "name": this.n, "dialogue": "Goeie keuze, deze schoenen heb ik zeker niet ergens van de grond geraapt", "type": "single", "chathead": this.l }
        }else if (stage == 100){
            return { "name": this.n, "dialogue": "Je hebt niet genoeg geld, wat jammer nou!", "type": "single", "chathead": this.l }   
        } else if (stage == 200 && inventory.contains(13)) {
            return { "name": this.n, "dialogue": "Je hebt al een pinda, pleur op", "type": "single", "chathead": this.l }
        } else if (stage == 200 && !inventory.contains(13)) {
            inventory.removeItem(coin.getId(), 1)
            inventory.add(peanut, 1)
            return { "name": this.n, "dialogue": "Veel succes met je pinda, malloot", "type": "single", "chathead": this.l }
        }
        else if (stage == 300) {
            inventory.removeItem(coin.getId(), 280000)
            return { "name": this.n, "dialogue": "Ha! stik maar in je EC's", "type": "single", "chathead": this.l }
        } else if (stage == 400 && inventory.contains(12)) {
            return { "name": this.n, "dialogue": "Je hebt al een doekje, pleur op", "type": "single", "chathead": this.l }

        } else if (stage == 400 && !inventory.contains(12)) {
            inventory.removeItem(coin.getId(), 1)
            inventory.add(cleaningCloth, 1)
            return { "name": this.n, "dialogue": "Veel succes met je doekje, malloot", "type": "single", "chathead": this.l }
        } else {
            return null;
        }
    }

}