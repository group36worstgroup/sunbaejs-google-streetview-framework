function Wortel() {
    this.n = "Arthur de boze wortel";
    this.nb = "Arthur de hele boze wortel";
    this.l = "image/Arthur_de_boze_wortel.png";
    this.lb = "image/Arthur_de_hele_boze_wortel.png";

    this.e = "image/leeg.png";

    this.dial = function (stage, npc) {
        if (stage == -1) {
            return { "name": this.n, "dialogue": "Hallo en welkom in de demo quest " + player.getName() + "! <br> Ik ben Arthur de boze wortel, ik moet jou op weg helpen", "type":"single", "chathead": this.l }
        } else if(stage == 0){
            return { "name":this.n, "dialogue":"Oh ja, ik moet natuurlijk wel boos zijn, dat is de bedoeling", "type":"single", "chathead":this.l}
        } else if(stage == 1){
            return { "name": "", "dialogue": "Arthur realiseert zich dat hij nog niet boos genoeg is.", "type": "single", "chathead": this.e }
        } else if(stage == 2){
            return { "name": this.nb, "dialogue":"Zo dat is beter, Leon is zijn Power Ranger pak kwijt, jij moet hem weer terug vinden voor Leon.", "type":"single", "chathead":this.lb}            
        } else if(stage == 3){
            return {"name":this.nb, "dialogue": "Leon is bang voor bussen dus heeft hij zich verscholen achter de school! hier heb je alvast de handschoenen van het Power Ranger pak, de rest ben ik kwijt!", "type":"single", "chathead":this.lb}
        }
        else if(stage == 4){
            inventory.add(powerRangerGloves, 1);
            return {"name":"", "dialogue":"Arthur geeft je de Power Ranger handschoenen.", "type":"single", "chathead": powerRangerGloves.getImgPath()}
        } else if(stage == 5){
            return{"name":this.nb, "dialogue":"Succes met het vinden van de rest!", "type":"single", "chathead": this.lb}
        }
        else {
            return null;
        }
    }

}