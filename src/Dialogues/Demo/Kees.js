function Kees() {
    this.l = "https://i.imgur.com/JXdnsen.png";

    this.dial = function (stage, npc) {
        if (stage == -1) {
            return { "name": "Kees", "dialogue": "Hoi ik ben Kees", "type": "single", "chathead": this.l }
        } else if (stage == 0 && inventory.contains(43594)) {
            return { "name": "Kees", "dialogue": "HEY JIJ HEBT EEN BANANA, DIE NEEM IK DANKJE HAHA! IK HOU VAN BANANEN 🍌 🍌 🍌 ", "type": "single", "chathead": this.l }
        } else if (stage == 1 && inventory.contains(43594)) {
            let gbName = inventory.getSlotOfItem(43594).getItem().getName();
            let gbChat = inventory.getSlotOfItem(43594).getItem().getImgPath();
            return { "name": gbName, "dialogue": "VIEZE HOND BLIJF VAN ME AF", "type": "single", "chathead": gbChat }
        } else if (stage == 2) {
            let gbName = inventory.getSlotOfItem(43594).getItem().getName();
            let gbChat = inventory.getSlotOfItem(43594).getItem().getImgPath();
            npc.remove({ "type": "death", "dropping": true, "dropTable": [{ "item": dodeKees, "amount": 1 }, { "item": banana, "amount": 5000 }] });
            return { "name": "", "dialogue": gbName + " heeft Kees neergeschoten", "type": "single", "chathead": gbChat }
        }
        else if (stage == 0 && inventory.contains(1)) {
            let nameOfItem = inventory.getSlotOfItem(1).getItem().getName();
            inventory.removeItem(1, 1);
            return {/*"type" : DialogueType.SINGLE, */"name": "Kees", "dialogue": "HEY JIJ HEBT EEN " + nameOfItem + ", DIE NEEM IK DANKJE HAHA! IK HOU VAN BANANEN 🍌 🍌 🍌 ", "type": "single", "chathead": this.l }        
        }else if (stage == 0 && inventory.contains(11)) {
            let nameOfItem = inventory.getSlotOfItem(11).getItem().getName();
            inventory.removeItem(11, 1);
            return {/*"type" : DialogueType.SINGLE, */"name": "Kees", "dialogue": "HEY JIJ HEBT EEN " + nameOfItem + ", DIE NEEM IK DANKJE HAHA! IK HOU VAN BANANEN 🍌 🍌 🍌 ", "type": "single", "chathead": this.l }
        }else if (stage == 0 && !inventory.contains(1)) {
            return { "name": "Kees", "dialogue": "Waarom heb jij geen bananen? ik wil bananen!", "type": "single", "chathead": this.l }
        } else {
            return null;
        }
    }

}