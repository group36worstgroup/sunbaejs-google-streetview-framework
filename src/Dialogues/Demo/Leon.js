function Leon() {
    this.n = "Leon";
    this.l = "image/leonchathead.png";
    this.lc = "image/huilende_leon.png";
    this.lb = "image/leonchatheadblij.png";

    this.dial = function (stage, npc) {
        if (stage == -1) {
            if (player.completedPowerRangerSet()) {
                return { "type": "setter", "newstage": 700 }
            }
            return { "name": this.n, "dialogue": "IK BEN MIJN POWER RANGER PAK KWIJT HELP ME HET TERUG TE VINDEN", "type": "single", "chathead": this.lc }
        } else if (stage == 0) {
            return { "name": this.n, "dialogue": "Alsjeblieft zeg dat je een gedeelte van mijn Power Ranger pak hebt", "type": "single", "chathead": this.lc }
        } else if (stage == 1) {
            return { "question": "Heb je een gedeelte van " + this.n + " zijn Power Ranger pak", "options": ["Ja", "Nee", "Check Power Ranger pak"], "stages": [100, 200, 600], "type": "multi", "chathead": powerRangerGloves.getImgPath() }
        } else if (stage == 100) { //answered yes
            if (inventory.contains(2)
                || inventory.contains(3)
                || inventory.contains(4)
                || inventory.contains(5)
                || inventory.contains(6)) {
                return { "name": this.n, "dialogue": "JE HEBT EEN GEDEELTE VAN MIJN PAK GEEF HET NU", "type": "single", "chathead": this.l, "newstage": 500 }
            }
            else {
                return { "name": this.n, "dialogue": "Waarom lieg je tegen mij..", "type": "single", "chathead": this.lc }
            }
        } else if (stage == 500) { //handing over
            let constructedString = "";
            let first = true;
            let arr = [];
            for (var i = 2; i < 7; i++) {
                if (inventory.contains(i)) {
                    if (first) {
                        constructedString = constructedString + "Je geeft Leon de " + inventory.getSlotOfItem(i).getItem().getName();
                        first = false;
                        arr.push(i);
                    } else {
                        constructedString = constructedString + " en de " + inventory.getSlotOfItem(i).getItem().getName();
                        arr.push(i);
                    }
                }
            }
            constructedString = constructedString + ".";
            for (var i = 0; i < arr.length; i++) {
                inventory.removeItem(arr[i], 1);
                player.gotRangerPiece(arr[i], true);
            }
            return { "name": "", "dialogue": constructedString, "type": "single", "chathead": this.lb }
        } else if (stage == 501) {
            if (player.completedPowerRangerSet()) {
                return { "type": "setter", "newstage": 700 }
            } else {
                return null;
            }
        } else if (stage == 600) {
            let c;
            if (player.getHandedOverHelm()) { c = "Leon heeft de Helm.<br>"; } else { c = "Leon heeft de Helm nog niet.<br>"; }
            if (player.getHandedOverBody()) { c = c + "Leon heeft de Body.<br>"; } else { c = c + "Leon heeft de Body nog niet.<br>"; }
            if (player.getHandedOverLegs()) { c = c + "Leon heeft de Legs.<br>"; } else { c = c + "Leon heeft de Legs nog niet.<br>"; }
            if (player.getHandedOverBoots()) { c = c + "Leon heeft de Boots.<br>" } else { c = c + "Leon heeft de Boots nog niet.<br>"; }
            if (player.getHandedOverGloves()) { c = c + "Leon heeft de Gloves.<br>" } else { c = c + "Leon heeft de Gloves nog niet.<br>"; }
            return { "name": "", "dialogue": "Je hebt de volgende stukken aan Leon gegeven:<br>" + c, "type": "single", "chathead": "image/leeg.png" }

        } else if (stage == 200) { //answered no
            return { "name": this.n, "dialogue": ":(", "type": "single", "chathead": this.l }
        }
        else if (stage == 700) {
            new Audio('Sound/powerRangers.mp3').play();
            return { "name": this.n, "dialogue": "Bedankt voor het terug brengen van mijn power ranger pak, ik ben je voor altijd dankbaar!", "type": "single", "chathead": this.l }
        } else if (stage == 701) {

            this.nbb = "ARTHUR DE HELE HELE BOZE WORTEL";
            this.lb = "image/arthur_de_hele_boze_wortel.png";
            return { "name": this.nbb, "dialogue": "Goed gedaan, je hebt Leon zijn Power Ranger pak terug gebracht, Leon is voor altijd een echte Power Ranger nu, en ik blijf voor altijd boos.", "type": "single", "chathead": this.lb }

        } else if (stage == 702) {

            sessionStorage.setItem("questStatus", "done");
            window.location.href = "overworld.html";
        }
        else {
            return null;
        }
    }
}