function Willem() {
    var n = "Willem";
    this.l = "http://vignette3.wikia.nocookie.net/clubpenguin/images/4/42/Snowman_sprite_002.png/revision/latest?cb=20161001011125";

    this.e = "image/leeg.png";

    this.dial = function (stage, npc) {
        if (stage == -1 && (inventory.contains(4) || player.getHandedOverLegs())){
            return { "name": n, "dialogue": "pleur op, je hebt de broek al", "type": "single", "chathead": this.l, "newstage": 200}
        }else if (stage == -1 && inventory.contains(13) && n == "Brede Willem") {
            return { "name": n, "dialogue": "Brede Willem is nergens bang voor! behalve pinda's...", "type": "single", "chathead": this.l }
        } else if (stage == -1 && inventory.contains(13) && n == "Willem") {
            return {"name": n, "dialogue": "Gadverdamme een pinda! daar ben ik allergisch voor, rot op!", "type": "single", "chathead": this.l }
        } else if (stage == -1 && n == "Brede Willem") {
            return { "name": n, "dialogue": "Wat mot je", "type": "single", "chathead": this.l }
        } else if (stage == -1) {
            return { "name": n, "dialogue": "Hoi ik ben Willem, Brede Willem", "type": "single", "chathead": this.l }
        } else if (stage == 0 && inventory.contains(13) && (!inventory.contains(4) || !player.getHandedOverLegs())){
            inventory.add(powerRangerLegs, 1);            
            return { "name": n, "dialogue": "oké oké jij wint! neem deze broek en flikker een eind op met je smeringe fucking pinda", "type": "single", "chathead": this.l }
        } else if (stage == 0 && !inventory.contains(13) && (!inventory.contains(4) || !player.getHandedOverLegs()))    {
            n = "Brede Willem";
            player.setbredeWillem(true);
            return { "name": n, "dialogue": "Wil je matten? Dat kan!", "type": "single", "chathead": this.l }
        }
        else if (stage == 1 && !inventory.contains(13)) {
            return { "question": "Wil je vechten?", "options": ["Altijd!", "Nee ik durf niet."], "stages": [100, 200], "type": "multi", "chathead": this.l }
        } else if (stage == 100) {
            return { "name": n, "dialogue": "KOM VECHTEN DAN!!", "type": "single", "chathead": this.l }
        } else if (stage == 101) {
            return { "name": "", "dialogue": "Jij en Willem gaan op de vuist!", "type": "single", "chathead": this.e }
        } else if (stage == 102 && inventory.contains(7) && inventory.contains(8)) {
            inventory.removeItem(ironSword.getId(), 1);
            inventory.removeItem(runeHelm.getId(), 1);
            inventory.add(powerRangerLegs, 1);
            return { "name": n, "dialogue": "GODVERDOMME wat een tering zooi, jij wint deze keer vieze tyfus jood!", "type": "single", "chathead": this.l }
        } else if (stage == 102 && inventory.contains(7) || stage == 102 && inventory.contains(8)) {
            return { "name": "", "dialogue": "Het was een goeie strijd maar je hebt meer nodig om willem te verslaan!", "type": "single", "chathead": this.e }
        } else if (stage == 102 && !inventory.contains(7) && !inventory.contains(8)) {
            return { "name": "", "dialogue": "Brede Willem sloopt je helemaal, je hebt wapens en een harnas nodig!", "type": "single", "chathead": this.e }
        } else if (stage == 103 && !inventory.contains(4)) {
            inventory.dropAll();
            return { "name": "", "dialogue": "Brede Willem heeft je zo hard aangepakt dat al je items op de grond zijn gevallen", "type": "single", "chathead": this.e }
        } else if (stage == 200) {
            return { "name": n, "dialogue": "Laffe brillenjood hahahah!", "type": "single", "chathead": this.l }
        }
        else {
            return null;
        }
    }

}