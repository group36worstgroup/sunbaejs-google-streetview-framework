function Sjaak() {
    this.n = "Sjaak";
    this.l = "image/SjaakChathead.gif";

    this.dial = function (stage) {
        if (stage == -1 && (inventory.contains(2) || player.getHandedOverHelm())){
            return {"name": this.n, "dialogue": "Ga weg, alsjeblieft.", "type": "single", "chathead": this.l, "newstage": -2  }
        } else if (stage == -1) {
            return { "name": this.n, "dialogue": "Goedendag mijn naam is Sjaak.", "type":"single", "chathead": this.l }
        } else if (stage == 0) {
            return { "name": this.n, "dialogue": "U heeft wellicht wel oor van mij gehad. Ik sta alom bekend als de amijm ridder met lange zwarte haren.", "type":"single", "chathead": this.l  }
        } else if (stage == 1
        && (inventory.contains(3) || player.getHandedOverBody())
        && (inventory.contains(4) || player.getHandedOverLegs())
        && (inventory.contains(5) || player.getHandedOverGloves())
        && (inventory.contains(6) || player.getHandedOverBoots())) {
            return { "question": "Ik zie dat je daadwerkelijk alle onderdelen van het pak op mijn onderdeel na hebt weten te verzamelen, ik ben onder de indruk! Aan de hand van deze prestatie aanschouw ik jou als een waardig persoon om mijn test te doorstaan! wil je nu beginnen?", "options": ["Graag", "Liever niet"], "stages": [100, 200], "type":"multi", "chathead": this.l  }
        } else if (stage == 1){
            return {"name": this.n, "dialogue": "Je wilt MIJN powerranger helm hebben? wat denk je wel niet jij heikneuter. scheer je weg en toon je gezicht niet weer totdat je zelf de andere vier onderdelen van het pak hebt verzameld!", "type": "single", "chathead": this.l}
        } else if (stage == 100) {
            return { "name": this.n, "dialogue": "Goed, laten we beginnen!", "type":"single", "chathead": this.l  }
        } else if (stage == 101){
            return {"question": "Nou dan de eerste vraag: Hoe groot is de standaard groep powerrangers?", "options": ["4", "5", "6"], "stages": [300, 300, 400], "type":"multi", "chathead": this.l  } 
        } else if (stage == 200) {
            return { "name": this.n, "dialogue": "Scheer je weg smerige proletariër!", "type":"single", "chathead": this.l  }
        } else if (stage == 300){
            return { "name": this.n, "dialogue": "Scheer je weg smerige onder ge-educeerde proletariër!","type":"single", "chathead": this.l  }
        } else if (stage == 400){
            return {"question": "Je antwoord was correct! dan nu de tweede vraag: Waarom verschuild Leon zich achter de school?", "options": ["Omdat hij bang is voor bussen", "Hij is mensenschuw", "door een sociaal trauma durft hij niet meer publiekelijk te gaan"], "stages": [500, 300, 300], "type": "multi", "chathead": this.l  }
        } else if (stage == 500){
            return {"question": "Poeh, alweer goed! dan nu de laatste vraag: Waarom heeft Fred zo'n hekel aan Kees?", "options": ["Omdat Kees zijn goblin-vrouw heeft verkracht", "Omdat Kees bananen heeft genakt van de goblin-natie", "Omdat Kees disrespectvolle kreten heeft geuit tegenover de goblinführer"], "stages": [300,600,300], "type": "multi", "chathead": this.l  }
        } else if (stage == 600){
            inventory.add(powerRangerHelm, 1);
            return { "name": this.n, "dialogue": "Zeer indrukwekkend! Je hebt hard gewerkt en verdient het Power Ranger masker. Hier is het masker, scheer je nu weg smerige proletariër!", "type":"single", "chathead": this.l  }
        }

        else {
            return null;
        }
    }

}