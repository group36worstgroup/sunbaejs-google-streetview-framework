/**
 * dis zeit meine gutte friend Fred ze Goblin
 * @author God
 * @author Petrus
 */
function Fred() {
    this.n = "Fred";
    this.l = "image/goblinchathead.png";

    this.dial = function (stage) {
        if (stage == -1) {
            return { "name": this.n, "dialogue": "Ich bin Fred der Goblin! haha", "type": "single", "chathead": this.l }
        }
        else if (stage == 0) {
            return { "name": this.n, "dialogue": "Alle Menschen müssen sterben!", "type": "single", "chathead": this.l }
        }
        else if (stage == 1) {
            return { "question": "Bringt mir Kees seinen Körper oder füttere ich Sie zu den Hunden", "options": ["Ja", "Nein"], "stages": [100, 200], "type":"multi", "chathead": this.l  }
        } else if (stage == 100) {
            return { "name": this.n, "dialogue": "Gut, zer Gut! heil der Goblin Fuhrer!", "type": "single", "chathead": this.l }
        } else if (stage == 101 && inventory.contains(3434)) {
            inventory.removeItem(dodeKees, 1);
            inventory.add(powerRangerBody);
            return { "name": this.n, "dialogue": "Ah! Danke, dass du mir die Leiche des Entarteten gebracht hast", "type": "single", "chathead": this.l }
        } else if (stage == 200) {
            return { "name": this.n, "dialogue": "SchweinHund, heil der Goblin Fuhrer!", "type": "single", "chathead": this.l }
        }
        else {
            return null;
        }
    }

}