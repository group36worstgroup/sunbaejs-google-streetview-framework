function Sjaak() {
    this.n = "Sjaak";
    this.l = "https://68.media.tumblr.com/avatar_8a31b94dd3e3_128.png";

    this.dial = function (stage) {
        if (stage == -1) {
            return { "name": this.n, "dialogue": "Goedendag mijn naam is Sjaak.", "type":"single", "chathead": this.l }
        }
        else if (stage == 0) {
            return { "name": this.n, "dialogue": "U heeft mischien wel van mij gehoord ik sta alom bekend als de anijm ridder met lang zwart haar.", "type":"single", "chathead": this.l  }
        }
        else if (stage == 1) {
            return { "question": "Zou jij ook geintresseerd zijn in een original pink powerranger costuum voor het costuumbal?", "options": ["Ja", "Nee"], "stages": [100, 200], "type":"multi", "chathead": this.l  }
        } else if (stage == 100) {
            return { "name": this.n, "dialogue": "Goed", "type":"single", "chathead": this.l  }
        } else if (stage == 200) {
            return { "name": this.n, "dialogue": "Scheer je weg smerige proletariërs!","type":"single", "chathead": this.l  }
        }
        else {
            return null;
        }
    }

}