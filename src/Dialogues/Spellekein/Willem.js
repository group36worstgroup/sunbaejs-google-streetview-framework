function Willem() {
    this.n = "Willem";
    this.l = "http://vignette3.wikia.nocookie.net/clubpenguin/images/4/42/Snowman_sprite_002.png/revision/latest?cb=20161001011125";

    this.dial = function (stage, npc) {
        if (stage == -1) {
            return { "name": this.n, "dialogue": "Hoi ik ben Willem", "type":"single", "chathead": this.l }
        }
        else if (stage == 0) {
            return { "name": this.n, "dialogue": "Wil je vechten? Dat kan!", "type":"single", "chathead": this.l  }
        }
        else if (stage == 1) {
            return { "question": "Wil je vechten?", "options": ["Ja", "Nee"], "stages": [100, 200], "type":"multi", "chathead": this.l  }
        } else if (stage == 100) {
            return { "name": this.n, "dialogue": "KOM VECHTEN DAN!!", "type":"single", "chathead": this.l  }
        } else if (stage == 200) {
            return { "name": this.n, "dialogue": "Watje hahahah!","type":"single", "chathead": this.l  }
        }
        else {
            return null;
        }
    }

}