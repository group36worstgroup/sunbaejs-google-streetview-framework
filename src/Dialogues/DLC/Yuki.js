function Yuki() {
    this.n = "Yuki :3";
    this.l = "image/bodypillow.png";

    this.e = "image/leeg.png";

    this.dial = function (stage, npc) {
        if (stage == -1) {
            return { "name": this.n, "dialogue": "Knoichiwa, ore wa yuki desu", "type":"single", "chathead": this.l }
        } 
        else {
            return null;
        }
    }

}