function ExamineItem(parItem, parAmount, parImgPath) {
    this.item = parItem;
    this.n = parItem.getName();
    this.a = parAmount;
    this.i = parImgPath;

    this.dial = function (stage, npc) {
        if (stage == -1) {


            if (this.a > 1) {
                return { "name": "", "dialogue": "Je hebt " + this.a + " " + this.n + "en" + "<br /><br />" + this.item.getDescription(), "type": "single", "chathead": this.i }
            } else {
                return { "name": "", "dialogue": "Je hebt een " + this.n + "<br /><br />" + this.item.getDescription(), "type": "single", "chathead": this.i }
            }
        }
        else {
            return null;
        }
    }

}