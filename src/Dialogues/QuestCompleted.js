function QuestCompleted(n) {
    let name = n;

        this.dial = function (stage, npc) {

            if (stage == -1) {
                return { "name": name, "dialogue": "je hebt " + name + " gehaald!", "type": "single", "chathead": "image/QuestSymbol.png" }
            } else {
                return null;
            }
        }
    }